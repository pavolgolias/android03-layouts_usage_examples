package layouts.tutorial.android.com.android03_layouts_usage_examples;

import android.app.Activity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.RadioGroup;


public class MainActivity extends Activity {
    RadioGroup orientation;
    RadioGroup gravity;
    RadioGroup.OnCheckedChangeListener listener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Uncomment the section you want to try

        /****Linear layout example******/
        /*setContentView(R.layout.activity_main);

        listener = new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId){
                    case R.id.horizontal:
                        orientation.setOrientation(LinearLayout.HORIZONTAL);
                        break;
                    case R.id.vertical:
                        orientation.setOrientation(LinearLayout.VERTICAL);
                        break;

                    case R.id.left:
                        gravity.setGravity(Gravity.LEFT);
                        break;
                    case R.id.center:
                        gravity.setGravity(Gravity.CENTER_HORIZONTAL);
                        break;
                    case R.id.right:
                        gravity.setGravity(Gravity.RIGHT);
                        break;
                }
            }
        };

        orientation = (RadioGroup) findViewById(R.id.orientation);
        gravity = (RadioGroup) findViewById(R.id.gravity);

        orientation.setOnCheckedChangeListener(listener);
        gravity.setOnCheckedChangeListener(listener);*/
        /****Linear layout example******/


        /****Relative layout example******/
        //setContentView(R.layout.activity_main_relative);
        /****Relative layout example******/

        /****Table layout witch scroll view example******/
        setContentView(R.layout.activity_main_scroll_table);
        /****Table layout witch scroll view example******/
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
